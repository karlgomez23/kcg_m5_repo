import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EquipoDAOImplementacio implements EquipoDAO {
    static Connection con = Connexio.getConnection();

    public int create(Equipo equipo) throws SQLException {
        String query = ("Insert INTO Equipos(nomEquipo)VALUES(?)");
        PreparedStatement ps = con.prepareStatement(query);
        ps.setString(1, equipo.getNomEquipo());
        ResultSet rs = ps.getGeneratedKeys();
        rs.next();
        int n = rs.getInt(1);
        return n;
    }

    public Equipo read(int equipoId) throws SQLException {
//        String query = "Select * from Equipo where id_equipo = ?";
//        PreparedStatement ps = con.prepareStatement(query);
//        ps.setInt(1, equipoId);
//        ResultSet rs = ps.executeQuery();
//        Equipo equipo = null;
//
//        if (rs.next()) {
//            equipo = new Equipo(rs.getString("nomEquipo"));
//        }
//        rs.close();
//        ps.close();
           return null;
    }


    @Override
    public List<Equipo> getEquipos() throws SQLException {
        String query = "SELECT * FROM Equipos";
        PreparedStatement ps = con.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        List<Equipo> listaDeEquipos = new ArrayList<>();
        while (rs.next()){
            Equipo equipo = new Equipo();
            equipo.setNomEquipo(rs.getString("nomEquipo"));
            listaDeEquipos.add(equipo);
        }
        return listaDeEquipos;
    }



    public boolean existeID(int equipoId) throws SQLException{
        Statement stmt = null;
        try {
            String query = "Select * from Equipo where equipoId = ?";
            PreparedStatement ps = con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                return true;
            }
        }catch (SQLException err){
            return Boolean.parseBoolean("ID no existente");
        }
        return false;
    }
}
