import java.sql.SQLException;
import java.util.List;

public interface JugadorDAO {
    public int create(Jugador jugador) throws SQLException;
    public Jugador read(int jugadorId) throws SQLException;
    public void update(Jugador jugador) throws SQLException;
    public void delete(int jugadorId) throws SQLException;
    public List<Jugador> getJugadors() throws SQLException;
}
